# Docker - Laravel - Apache - MySql


-   Laravel 8
-   PHP 8
-   MySql 5.7

## Description

Application laravel avec `docker` en utilisant `docker-compose`.

l'image utilisé est le repository `php:8.0-apache` aet `mysql:5.7`. Le but c'est de mettre un envirennemnt de developpement le plus simple possible et plus rapide.

Laravel configuration avec docker 

#### Installation
cloner projet puis:

```
cd laravel-app

```
#### .env
Dans .env le DB_HOST doit etre egale au nom de service Mysql ici "mysql-db"
Copier le contenu de .env.example dans un fichier .en

```
cp .env.example .env

```

Build images et start services:

```
docker-compose build
docker-compose up -d
```

#### Connecter aux conteeurs

```
docker exec -it laravel-app bash -c "sudo -u devuser /bin/bash"

composer install

php artisan key:generate

```

#### Connecter au BD mysql

```
docker exec -it mysql-db bash -c "mysql -ularavel -ppassword"

```

#### migration

```
docker exec -it laravel-app bash -c "sudo -u devuser /bin/bash"

php artisan migrate

```

Server URL:

http://localhost:8080/
